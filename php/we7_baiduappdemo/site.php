<?php
/**
 * we7_baiduappdemo模块微站定义
 *
 * @author 微擎团队
 * @url
 */
defined('IN_IA') or exit('Access Denied');

class We7_baiduappdemoModuleSite extends WeModuleSite {

	public function doWebManage() {
		global $_W, $_GPC;
		$total = pdo_getcolumn('we7_baiduappdemo_message', array(), 'count(*)');
		include $this->template('manage');
	}

	public function doWebDelMsg() {
		pdo_run('TRUNCATE ' . tablename('we7_baiduappdemo_message'));
		itoast('', $this->createWebUrl('manage'));
	}

}