<?php
/**
 * we7_baiduappdemo百度小程序接口定义
 *
 * @author 微擎团队
 * @url
 */
defined('IN_IA') or exit('Access Denied');

class We7_baiduappdemoModuleBaiduapp extends WeModuleBaiduapp {
	public function doPageTest(){
		global $_GPC, $_W;
		$errno = 0;
		$message = '返回消息';
		$data = array();
		return $this->result($errno, $message, $data);
	}

	public function doPageMessageList(){
		global $_GPC, $_W;
		$data = pdo_getall('we7_baiduappdemo_message', array('uniacid' => $_W['uniacid']), array(), '', 'id desc', 6);
		if (!empty($data)) {
			foreach ($data as &$item) {
				if (strlen($item['message']) > 16) {
					$item['message'] = mb_substr($item['message'], 0, 16) . '...';
				}
			}
		}
		return $this->result(0, '', $data);
	}

	public function doPageAddMessage(){
		global $_GPC, $_W;
		if (!empty($_GPC['message'])) {
			$result = pdo_insert('we7_baiduappdemo_message', array(
				'nickname' => safe_gpc_string($_GPC['nickname']),
				'message' => safe_gpc_string($_GPC['message']),
				'uniacid' => safe_gpc_string($_W['uniacid']),
			));
		}
		return $this->result(empty($result) ? -1 : 0, '');
	}
}