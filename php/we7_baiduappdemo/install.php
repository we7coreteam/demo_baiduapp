<?php
$tables = array(
	'we7_baiduappdemo_message' => "
		CREATE TABLE IF NOT EXISTS " . tablename('we7_baiduappdemo_message') . " (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`uniacid` int(10) unsigned NOT NULL,
			`nickname` varchar(100) NOT NULL DEFAULT '',
			`message` varchar(500) NOT NULL DEFAULT '',
			PRIMARY KEY (`id`),
			KEY `uniacid` (`uniacid`)
		) DEFAULT CHARSET=utf8;
	",
);
foreach ($tables as $table_name => $sql) {
	pdo_run($sql);
}