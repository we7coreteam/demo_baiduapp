Page({
    data: {
        nickname: '昵称',
        imageSrc: '../../images/avator.png',
        nameColor: 'default',
        content: '',
    },
    formSubmitHandle(e) {
        if (!e.detail.value.message) {
            swan.showToast({
                title: "请先输入留言内容", 
                icon: 'none'
            });
            return false;
        }
        var app = getApp();
        var that = this;
        var postData = {
            nickname: this.getData('nickname'),
            message: e.detail.value.message 
        };
        
        swan.request({
            url: app.globalData.url + 'do=addMessage',
            method: 'POST',
            dataType: 'json',
            data: postData,
            header: {
                'content-type': 'application/x-www-form-urlencoded'
            },
            success: function (res) {
                if (!res.data.errno) {
                    swan.showToast({ 
                        title: '留言成功！'
                    });
                    that.setData({
                        content: '',
                    })
                } else {
                    swan.showToast({
                        title: res.data.message || "留言失败，请重试..", 
                        icon: 'none'
                    });
                }
            },
            fail: function (err) {
                console.log(err);
            }
        });
    },
    onShow(context) {
        swan.getUserInfo({
            success: res => {
                let userInfo = res.userInfo;
                this.setData({
                    nickname: userInfo.nickName || '昵称',
                    imageSrc: userInfo.avatarUrl || '../../images/avator.png',
                    nameColor: 'active'
                });
            },
            fail: err => {
                console.log(err);
                swan.showToast({
                    title: '请先授权'
                });
            }
        });
    }
});

swan.setPageInfo && swan.setPageInfo({
    'title': 'abc',
});