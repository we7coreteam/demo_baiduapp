Page({
    data: {
        items: [
            {
                className: 'color-a',
                value: '微擎示例'
            }, {
                className: 'color-b',
                value: '微擎示例'
            }
        ],
        current: 0,
        switchIndicateStatus: true,
        switchAutoPlayStatus: false,
        switchDuration: 500,
        autoPlayInterval: 2000,
        list: []
    },
    loadList() {
        var app = getApp();
        var that = this;
        swan.request({
            url: app.globalData.url + 'do=messageList',
            method: 'GET',
            dataType: 'json',
            data: {},
            success: function (res) {
                var data = res.data.data || [];
                if (data.length < 1) {
                    that.setData({
                        list: [{nickname: '', message: '暂无留言'}]
                    })
                } else {
                    that.setData({
                        list: res.data.data
                    })
                }
            },
            fail: function (err) {
                console.log('错误码：' + err.errCode);
                console.log('错误信息：' + err.errMsg);
            }
        });
    },
    onShow() {
        this.loadList();
    }
});
