var siteInfo = require('./siteinfo.js');

App({
    onLaunch: function () {
        console.log('SWAN launch');
    },

    onShow: function () {
        console.log('SWAN展现');
    },

    onHide: function () {
        console.log('SWAN当前处于后台');
    },

    onError: function () {
        console.log('SWAN发生错误');
    },

    globalData: {
        userInfo: 'user',
        siteInfo: siteInfo,
        url: siteInfo.siteroot + '?i=' + siteInfo.uniacid + '&t=' + siteInfo.multiid + '&v=' + siteInfo.version + '&m=we7_baiduappdemo&c=entry&a=baiduapp&from=baiduapp&',
    }
});
