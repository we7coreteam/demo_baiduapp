# 文件结构
模块标识 we7_baiduappdemo

## js
> 百度智能小程序源码目录

### 百度智能小程序源码文件目录结构
- we7_baiduappdemo //模块标识
- - images 图片资源
- - we7pages
- - app.css
- - app.js
- - app.json
- - siteinfo.js

## php
> 百度智能小程序后台源码
#### 目录结构
- we7_baiduappdemo
- - template 视图模板目录
- - baiduapp.php 百度智能小程序前台接口入口
- - icon.jpg
- - install.php
- - manifest.xml
- - module.php  [说明文档](https://wiki.w7.cc/document/35/1528) 
- - preview.jpg
- - site.php  [说明文档](https://wiki.w7.cc/document/35/1529)
- - uninstall.php
 